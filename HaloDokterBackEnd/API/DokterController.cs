﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HaloDokterBackEnd.Models;
using HaloDokterBackEnd.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HaloDokterBackEnd.API
{
    [Route("api/[controller]")]
    public class DokterController : Controller
    {
        private readonly DokterServices DokterMan;
        public DokterController(DokterServices dokterService)
        {
            this.DokterMan = dokterService;
        }

        [HttpPost("insert-dokter", Name = "insert-dokter")]
        public async Task<ActionResult<bool>> InsertDokter([FromBody] DokterFormModel model)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest("ModelState is not valid");
            }

            var result = await this.DokterMan.InsertDoktor(model);
            return result;
        }

        [HttpGet("get-all-dokter", Name = "get-all-dokter")]
        public async Task<ActionResult<List<DokterModel>>> GetAllDokter()
        {
            var result = await this.DokterMan.GetAllDokter();

            return Ok(result);
        }

        [HttpGet("get-dokter-by-name/{name}", Name = "get-dokter-by-name")]
        public async Task<ActionResult<DokterModel>> GetDokterByName(string name)
        {
            var result = await this.DokterMan.GetDokterByName(name);

            return Ok(result);
        }

    }
}
