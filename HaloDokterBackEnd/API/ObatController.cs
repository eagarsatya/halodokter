﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HaloDokterBackEnd.Models;
using HaloDokterBackEnd.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HaloDokterBackEnd.API
{
    [Route("api/[controller]")]
    public class ObatController : Controller
    {
        private readonly ObatServices ObatMan;
        public ObatController(ObatServices obatServices)
        {
            this.ObatMan = obatServices;
        }

        [HttpPost("insert-obat", Name = "insert-obat")]
        public async Task<ActionResult<bool>> InsertObat([FromBody] ObatFormModel model)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest("Model State is not valid");
            }

            var result = await this.ObatMan.InsertObat(model);

            return Ok(result);
        }

        [HttpGet("get-all-obat", Name = "get-all-obat")]
        public async Task<ActionResult<List<ObatModel>>> GetAllObat()
        {
            var result = await this.ObatMan.GetAllObat();

            return Ok(result);
        }

        [HttpGet("get-obat-by-name/{name}", Name = "get-obat-by-name")]
        public async Task<ActionResult<ObatModel>> GetObatByName(string name)
        {
            var result = await this.ObatMan.GetObatByName(name);

            return Ok(result);
        }
    }
}
