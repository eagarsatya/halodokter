﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HaloDokterBackEnd.Models;
using HaloDokterBackEnd.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HaloDokterBackEnd.API
{
    [Route("api/[controller]")]
    public class PasienController : Controller
    {
        private readonly PasienServices PasienMan;
        public PasienController(PasienServices pasienServices)
        {
            this.PasienMan = pasienServices;
        }

        [HttpPost("insert-pasien", Name = "insert-pasien")]
        public async Task<ActionResult<bool>> InsertPasien([FromBody] PasienFormModel model)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest("Model State is not valid");
            }

            var result = await this.PasienMan.InsertPatient(model);

            return Ok(result);
        }

        [HttpGet("get-all-patient", Name = "get-all-patient")]
        public async Task<ActionResult<List<PasienModel>>> GetAllPatient()
        {
            var result = await this.PasienMan.GetAllPasient();
            return Ok(result);
        }

        [HttpGet("get-patient-by-name/{name}", Name = "get-patient-by-name")]
        public async Task<ActionResult<PasienModel>> GetPatientByName(string name)
        {
            var result = await this.PasienMan.GetPasientByName(name);
            return Ok(result);
        }
    }
}
