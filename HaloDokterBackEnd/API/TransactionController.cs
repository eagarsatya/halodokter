﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HaloDokterBackEnd.Models;
using HaloDokterBackEnd.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HaloDokterBackEnd.API
{
    [Route("api/[controller]")]
    public class TransactionController : Controller
    {
        private readonly TransactionServices TransactionMan;
        public TransactionController(TransactionServices transactionMan)
        {
            this.TransactionMan = transactionMan;
        }

        [HttpPost("insert-transaction", Name = "insert-transaction")]
        public async Task<ActionResult<bool>> InsertTransaction([FromBody] TransactionFormModel model)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest("Model State is not valid");
            }

            var result = await this.TransactionMan.InsertTransaction(model);

            return Ok(result);
        }

        [HttpGet("get-transaction-by-pasien-name/{name}", Name = "get-transaction-by-pasien-name")]
        public async Task<ActionResult<PasienObatModel>> GetTransactionByPasienName(string name)
        {
            var result = await this.TransactionMan.GetAllObat(name);

            return Ok(result);
        }
    }
}
