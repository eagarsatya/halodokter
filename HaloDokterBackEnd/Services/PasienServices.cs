﻿using HaloDokterBackEnd.Models;
using HaloDokterEntities.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HaloDokterBackEnd.Services
{
    public class PasienServices
    {
        private readonly HaloDokterContext DB;
        public PasienServices(HaloDokterContext haloDokterContext)
        {
            this.DB = haloDokterContext;
        }

        public async Task<bool> InsertPatient(PasienFormModel model)
        {
            var newPasien = new Pasien
            {
                NamaKerabat = model.NamaKerabat,
                NomorTelpon = model.NomorTelpon,
                NoTelponKerabat = model.NoTelponKerabat,
                PasienName = model.PasienName,
                PenyakitPasien = model.PenyakitPasien
            };

            this.DB.Add(newPasien);
            await this.DB.SaveChangesAsync();

            return true;
        }

        public async Task<List<PasienModel>> GetAllPasient()
        {
            var newPasien = await this.DB.Pasien.Select(Q => new PasienModel
            {
                NamaKerabat = Q.NamaKerabat,
                NomorTelpon = Q.NomorTelpon,
                NoTelponKerabat = Q.NoTelponKerabat,
                PasienId = Q.PasienId,
                PasienName = Q.PasienName,
                PenyakitPasien = Q.PenyakitPasien
            }).ToListAsync();

            return newPasien;
        }

        public async Task<PasienModel> GetPasientByName(string name)
        {
            var newPasien = await this.DB.Pasien.Where(Q => Q.PasienName.ToLower() == name.ToLower()).Select(Q => new PasienModel
            {
                NamaKerabat = Q.NamaKerabat,
                NomorTelpon = Q.NomorTelpon,
                NoTelponKerabat = Q.NoTelponKerabat,
                PasienId = Q.PasienId,
                PasienName = Q.PasienName,
                PenyakitPasien = Q.PenyakitPasien
            }).FirstOrDefaultAsync();

            return newPasien;
        }
    }
}
