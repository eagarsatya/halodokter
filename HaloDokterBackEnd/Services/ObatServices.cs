﻿using HaloDokterBackEnd.Models;
using HaloDokterEntities.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HaloDokterBackEnd.Services
{
    public class ObatServices
    {
        private readonly HaloDokterContext DB;
        public ObatServices(HaloDokterContext haloDokterContext)
        {
            this.DB = haloDokterContext;
        }

        public async Task<bool> InsertObat(ObatFormModel model)
        {
            var newObat = new Obat
            {
                ObatDescr = model.ObatDescr,
                ObatExpired = model.ObatExpired,
                ObatName = model.ObatName
            };

            this.DB.Obat.Add(newObat);
            await this.DB.SaveChangesAsync();

            return true;
        }

        public async Task<List<ObatModel>> GetAllObat()
        {
            var result = await this.DB.Obat.Select(Q => new ObatModel
            {
                ObatDescr = Q.ObatDescr,
                ObatExpired = Q.ObatExpired,
                ObatId = Q.ObatId,
                ObatName = Q.ObatName
            }).ToListAsync();

            return result;
        }

        public async Task<ObatModel> GetObatByName(string name)
        {
            var result = await this.DB.Obat.Where(Q => Q.ObatName.ToLower() == name.ToLower()).Select(Q => new ObatModel
            {
                ObatDescr = Q.ObatDescr,
                ObatExpired = Q.ObatExpired,
                ObatId = Q.ObatId,
                ObatName = Q.ObatName
            }).FirstOrDefaultAsync();

            return result;
        }

    }
}
