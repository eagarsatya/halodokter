﻿using HaloDokterBackEnd.Models;
using HaloDokterEntities.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HaloDokterBackEnd.Services
{
    public class TransactionServices
    {
        private readonly HaloDokterContext DB;
        public TransactionServices(HaloDokterContext haloDokterContext)
        {
            this.DB = haloDokterContext;
        }

        public async Task<bool> InsertTransaction(TransactionFormModel model)
        {
            var newAkses = new Akses
            {
                DokterId = model.DokterId,
                PasienId = model.PasienId
            };

            this.DB.Akses.Add(newAkses);
            await this.DB.SaveChangesAsync();

            var newReminder = new Reminder
            {
                AksesId = newAkses.AksesId,
                ReminderTime = model.ReminderTime,
                ReminderTitle = model.ReminderTitle
            };

            this.DB.Reminder.Add(newReminder);
            await this.DB.SaveChangesAsync();

            var newKonsumsiObat = new KonsumsiObat
            {
                KonsumsiObatDosis = model.KonsumsiObatDosis,
                KonsumsiObatStock = model.KonsumsiObatStock,
                KonsumsiObatTime = model.KonsumsiObatTime,
                ReminderId = newReminder.ReminderId,
                ObatId = model.ObatId
            };

            this.DB.KonsumsiObat.Add(newKonsumsiObat);
            await this.DB.SaveChangesAsync();

            return true;
        }

        public async Task<PasienObatModel> GetAllObat(string name)
        {
            var pasien = await this.DB.Pasien.Where(Q => Q.PasienName.ToLower() == name.ToLower()).FirstOrDefaultAsync();

            var newPasienObat = new PasienObatModel();

            if (pasien.PasienId == 0 && pasien == null)
            {
                return newPasienObat;
            }

            //var akses = await this.DB.Akses.Where(Q => Q.PasienId == pasien.PasienId).ToListAsync();

            //var listAksesIds = akses.Select(Q => Q.AksesId).ToList();

            //var reminders = await this.DB.Reminder.Where(Q => listAksesIds.Contains(Q.AksesId)).ToListAsync();

            //var reminderIds = reminders.Select(Q => Q.ReminderId).ToList();

            //var konsumsiObat = await this.DB.KonsumsiObat.Where(Q => reminderIds.Contains(Q.ReminderId)).ToListAsync();

            //newPasienObat.PasienId = pasien.PasienId;
            //newPasienObat.PasienName = pasien.PasienName;

            var result = await (from a in this.DB.Akses
                                join d in this.DB.Dokter on a.DokterId equals d.DokterId
                                join r in this.DB.Reminder on a.AksesId equals r.AksesId
                                join k in this.DB.KonsumsiObat on r.ReminderId equals k.ReminderId
                                join o in this.DB.Obat on k.ObatId equals o.ObatId
                                where a.PasienId == pasien.PasienId
                                select new TransactionObatModel
                                {
                                    DokterId = d.DokterId,
                                    DokterName = d.DokterName,
                                    KonsumsiObatDosis = k.KonsumsiObatDosis,
                                    KonsumsiObatTime = k.KonsumsiObatTime,
                                    ObatExpired = o.ObatExpired,
                                    ObatId = o.ObatId,
                                    ObatName = o.ObatName
                                }).ToListAsync();
            newPasienObat.PasienId = pasien.PasienId;
            newPasienObat.PasienName = pasien.PasienName;
            newPasienObat.ListTransaction = result;

            return newPasienObat;
        }
    }
}
