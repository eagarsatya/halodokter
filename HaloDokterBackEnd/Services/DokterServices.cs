﻿using HaloDokterBackEnd.Models;
using HaloDokterEntities.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HaloDokterBackEnd.Services
{
    public class DokterServices
    {
        private readonly HaloDokterContext DB;
        public DokterServices(HaloDokterContext haloDokterContext)
        {
            this.DB = haloDokterContext;
        }

        public async Task<bool> InsertDoktor(DokterFormModel model)
        {
            var newDokter = new Dokter
            {
                DokterName = model.DokterName,
                DokterSpesialis = model.DokterSpesialis,
            };

            this.DB.Dokter.Add(newDokter);
            await this.DB.SaveChangesAsync();

            return true;
        }

        public async Task<List<DokterModel>> GetAllDokter()
        {
            var allDokter = await this.DB.Dokter.Select(Q => new DokterModel
            {
                DokterId = Q.DokterId,
                DokterName = Q.DokterName,
                DokterSpesialis = Q.DokterSpesialis,
            }).ToListAsync();

            return allDokter;
        }

        public async Task<DokterModel> GetDokterByName(string name)
        {
            var allDokter = await this.DB.Dokter.Where(Q => Q.DokterName.ToLower() == name.ToLower()).Select(Q => new DokterModel
            {
                DokterId = Q.DokterId,
                DokterName = Q.DokterName,
                DokterSpesialis = Q.DokterSpesialis,
            }).FirstOrDefaultAsync();

            return allDokter;
        }

    }
}
