﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HaloDokterBackEnd.Models
{
    public class DokterModel
    {
        public int DokterId { get; set; }
        public string DokterName { get; set; }
        public string DokterSpesialis { get; set; }

    }

    public class DokterFormModel
    {
        public int DokterId { get; set; }
        [StringLength(200)]
        public string DokterName { get; set; }
        [StringLength(50)]
        public string DokterSpesialis { get; set; }

    }
}
