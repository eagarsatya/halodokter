﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HaloDokterBackEnd.Models
{
    public class ObatModel
    {
        public int ObatId { get; set; }
        public string ObatName { get; set; }
        public string ObatExpired { get; set; }
        public string ObatDescr { get; set; }
    }

    public class ObatFormModel
    {
        public int ObatId { get; set; }
        [Required]
        [StringLength(150)]
        public string ObatName { get; set; }
        [StringLength(200)]
        public string ObatExpired { get; set; }
        [StringLength(250)]
        public string ObatDescr { get; set; }
    }
}
