﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HaloDokterBackEnd.Models
{
    public class PasienModel
    {
        public int PasienId { get; set; }
        public string PasienName { get; set; }
        public string PenyakitPasien { get; set; }
        public int? NomorTelpon { get; set; }
        public string NamaKerabat { get; set; }
        public int? NoTelponKerabat { get; set; }
    }

    public class PasienFormModel
    {
        public int PasienId { get; set; }
        [Required]
        [StringLength(250)]
        public string PasienName { get; set; }
        [StringLength(200)]
        public string PenyakitPasien { get; set; }
        public int? NomorTelpon { get; set; }
        [StringLength(200)]
        public string NamaKerabat { get; set; }
        public int? NoTelponKerabat { get; set; }
    }
}
