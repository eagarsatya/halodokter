﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HaloDokterBackEnd.Models
{
    public class KonsumsiObatModel
    {
        public int KonsumsiObatId { get; set; }
        public int? KonsumsiObatStock { get; set; }
        public string KonsumsiObatTime { get; set; }
        public int? KonsumsiObatDosis { get; set; }
        public int ReminderId { get; set; }
    }

    public class KonsumsiObatFormModel
    {
        public int KonsumsiObatId { get; set; }
        public int? KonsumsiObatStock { get; set; }
        [StringLength(200)]
        public string KonsumsiObatTime { get; set; }
        public int? KonsumsiObatDosis { get; set; }
        public int ReminderId { get; set; }
    }
}
