﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HaloDokterBackEnd.Models
{
    public class ReminderModel
    {
        public int ReminderId { get; set; }
        public string ReminderTitle { get; set; }
        public string ReminderTime { get; set; }
        public int AksesId { get; set; }
    }

    public class ReminderFormModel
    {
        public int ReminderId { get; set; }
        [StringLength(50)]
        public string ReminderTitle { get; set; }
        [StringLength(50)]
        public string ReminderTime { get; set; }
        public int AksesId { get; set; }
    }
}
