﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace HaloDokterEntities.Entities
{
    public partial class HaloDokterEntities : DbContext
    {
        public HaloDokterEntities()
        {
        }

        public HaloDokterEntities(DbContextOptions<HaloDokterEntities> options)
            : base(options)
        {
        }

        public virtual DbSet<Akses> Akses { get; set; }
        public virtual DbSet<Dokter> Dokter { get; set; }
        public virtual DbSet<KonsumsiObat> KonsumsiObat { get; set; }
        public virtual DbSet<Obat> Obat { get; set; }
        public virtual DbSet<Pasien> Pasien { get; set; }
        public virtual DbSet<Reminder> Reminder { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=DESKTOP-POJTNUO;Database=HaloDokter;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.3-servicing-35854");

            modelBuilder.Entity<Akses>(entity =>
            {
                entity.HasOne(d => d.Dokter)
                    .WithMany(p => p.Akses)
                    .HasForeignKey(d => d.DokterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Akses__DokterId__2A4B4B5E");

                entity.HasOne(d => d.Pasien)
                    .WithMany(p => p.Akses)
                    .HasForeignKey(d => d.PasienId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Akses__PasienId__2B3F6F97");
            });

            modelBuilder.Entity<Dokter>(entity =>
            {
                entity.Property(e => e.DokterName).IsUnicode(false);

                entity.Property(e => e.DokterSpesialis).IsUnicode(false);
            });

            modelBuilder.Entity<KonsumsiObat>(entity =>
            {
                entity.Property(e => e.KonsumsiObatTime).IsUnicode(false);

                entity.HasOne(d => d.Reminder)
                    .WithMany(p => p.KonsumsiObat)
                    .HasForeignKey(d => d.ReminderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__KonsumsiO__Remin__30F848ED");
            });

            modelBuilder.Entity<Obat>(entity =>
            {
                entity.Property(e => e.ObatDescr).IsUnicode(false);

                entity.Property(e => e.ObatExpired).IsUnicode(false);

                entity.Property(e => e.ObatName).IsUnicode(false);
            });

            modelBuilder.Entity<Pasien>(entity =>
            {
                entity.Property(e => e.NamaKerabat).IsUnicode(false);

                entity.Property(e => e.PasienName).IsUnicode(false);

                entity.Property(e => e.PenyakitPasien).IsUnicode(false);
            });

            modelBuilder.Entity<Reminder>(entity =>
            {
                entity.Property(e => e.ReminderTime).IsUnicode(false);

                entity.Property(e => e.ReminderTitle).IsUnicode(false);

                entity.HasOne(d => d.Akses)
                    .WithMany(p => p.Reminder)
                    .HasForeignKey(d => d.AksesId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Reminder__AksesI__2E1BDC42");
            });
        }
    }
}
