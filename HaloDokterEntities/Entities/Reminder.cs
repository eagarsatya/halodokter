﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HaloDokterEntities.Entities
{
    public partial class Reminder
    {
        public Reminder()
        {
            KonsumsiObat = new HashSet<KonsumsiObat>();
        }

        public int ReminderId { get; set; }
        [StringLength(50)]
        public string ReminderTitle { get; set; }
        [StringLength(50)]
        public string ReminderTime { get; set; }
        public int AksesId { get; set; }

        [ForeignKey("AksesId")]
        [InverseProperty("Reminder")]
        public virtual Akses Akses { get; set; }
        [InverseProperty("Reminder")]
        public virtual ICollection<KonsumsiObat> KonsumsiObat { get; set; }
    }
}
