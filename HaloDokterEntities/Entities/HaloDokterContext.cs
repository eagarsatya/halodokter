﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace HaloDokterEntities.Entities
{
    public partial class HaloDokterContext : DbContext
    {
        public HaloDokterContext(DbContextOptions<HaloDokterContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Akses> Akses { get; set; }
        public virtual DbSet<Dokter> Dokter { get; set; }
        public virtual DbSet<KonsumsiObat> KonsumsiObat { get; set; }
        public virtual DbSet<Obat> Obat { get; set; }
        public virtual DbSet<Pasien> Pasien { get; set; }
        public virtual DbSet<Reminder> Reminder { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.3-servicing-35854");

            modelBuilder.Entity<Akses>(entity =>
            {
                entity.HasOne(d => d.Dokter)
                    .WithMany(p => p.Akses)
                    .HasForeignKey(d => d.DokterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Akses__DokterId__29572725");

                entity.HasOne(d => d.Pasien)
                    .WithMany(p => p.Akses)
                    .HasForeignKey(d => d.PasienId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Akses__PasienId__2A4B4B5E");
            });

            modelBuilder.Entity<Dokter>(entity =>
            {
                entity.Property(e => e.DokterName).IsUnicode(false);

                entity.Property(e => e.DokterSpesialis).IsUnicode(false);
            });

            modelBuilder.Entity<KonsumsiObat>(entity =>
            {
                entity.Property(e => e.KonsumsiObatTime).IsUnicode(false);

                entity.HasOne(d => d.Obat)
                    .WithMany(p => p.KonsumsiObat)
                    .HasForeignKey(d => d.ObatId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__KonsumsiO__ObatI__33D4B598");

                entity.HasOne(d => d.Reminder)
                    .WithMany(p => p.KonsumsiObat)
                    .HasForeignKey(d => d.ReminderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__KonsumsiO__Remin__32E0915F");
            });

            modelBuilder.Entity<Obat>(entity =>
            {
                entity.Property(e => e.ObatDescr).IsUnicode(false);

                entity.Property(e => e.ObatExpired).IsUnicode(false);

                entity.Property(e => e.ObatName).IsUnicode(false);
            });

            modelBuilder.Entity<Pasien>(entity =>
            {
                entity.Property(e => e.NamaKerabat).IsUnicode(false);

                entity.Property(e => e.PasienName).IsUnicode(false);

                entity.Property(e => e.PenyakitPasien).IsUnicode(false);
            });

            modelBuilder.Entity<Reminder>(entity =>
            {
                entity.Property(e => e.ReminderTime).IsUnicode(false);

                entity.Property(e => e.ReminderTitle).IsUnicode(false);

                entity.HasOne(d => d.Akses)
                    .WithMany(p => p.Reminder)
                    .HasForeignKey(d => d.AksesId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Reminder__AksesI__2D27B809");
            });
        }
    }
}
