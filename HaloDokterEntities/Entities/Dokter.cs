﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HaloDokterEntities.Entities
{
    public partial class Dokter
    {
        public Dokter()
        {
            Akses = new HashSet<Akses>();
        }

        public int DokterId { get; set; }
        [StringLength(200)]
        public string DokterName { get; set; }
        [StringLength(50)]
        public string DokterSpesialis { get; set; }

        [InverseProperty("Dokter")]
        public virtual ICollection<Akses> Akses { get; set; }
    }
}
