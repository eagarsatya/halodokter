﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HaloDokterEntities.Entities
{
    public partial class Akses
    {
        public Akses()
        {
            Reminder = new HashSet<Reminder>();
        }

        public int AksesId { get; set; }
        public int PasienId { get; set; }
        public int DokterId { get; set; }

        [ForeignKey("DokterId")]
        [InverseProperty("Akses")]
        public virtual Dokter Dokter { get; set; }
        [ForeignKey("PasienId")]
        [InverseProperty("Akses")]
        public virtual Pasien Pasien { get; set; }
        [InverseProperty("Akses")]
        public virtual ICollection<Reminder> Reminder { get; set; }
    }
}
