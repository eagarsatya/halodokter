﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HaloDokterEntities.Entities
{
    public partial class Pasien
    {
        public Pasien()
        {
            Akses = new HashSet<Akses>();
        }

        public int PasienId { get; set; }
        [Required]
        [StringLength(250)]
        public string PasienName { get; set; }
        [StringLength(200)]
        public string PenyakitPasien { get; set; }
        public int? NomorTelpon { get; set; }
        [StringLength(200)]
        public string NamaKerabat { get; set; }
        public int? NoTelponKerabat { get; set; }

        [InverseProperty("Pasien")]
        public virtual ICollection<Akses> Akses { get; set; }
    }
}
