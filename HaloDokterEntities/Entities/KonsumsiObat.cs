﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HaloDokterEntities.Entities
{
    public partial class KonsumsiObat
    {
        public int KonsumsiObatId { get; set; }
        public int? KonsumsiObatStock { get; set; }
        [StringLength(200)]
        public string KonsumsiObatTime { get; set; }
        public int? KonsumsiObatDosis { get; set; }
        public int ReminderId { get; set; }
        public int ObatId { get; set; }

        [ForeignKey("ObatId")]
        [InverseProperty("KonsumsiObat")]
        public virtual Obat Obat { get; set; }
        [ForeignKey("ReminderId")]
        [InverseProperty("KonsumsiObat")]
        public virtual Reminder Reminder { get; set; }
    }
}
