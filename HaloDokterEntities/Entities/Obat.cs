﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HaloDokterEntities.Entities
{
    public partial class Obat
    {
        public Obat()
        {
            KonsumsiObat = new HashSet<KonsumsiObat>();
        }

        public int ObatId { get; set; }
        [Required]
        [StringLength(150)]
        public string ObatName { get; set; }
        [StringLength(200)]
        public string ObatExpired { get; set; }
        [StringLength(250)]
        public string ObatDescr { get; set; }

        [InverseProperty("Obat")]
        public virtual ICollection<KonsumsiObat> KonsumsiObat { get; set; }
    }
}
